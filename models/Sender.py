import hashlib
from itertools import chain, repeat

from x256 import x256
import colored

from JanetNeuron import JanetNeuron


class Sender(JanetNeuron):
    _user = None

    @property
    def is_bot(self):
        if (
            "subtype" in self.source_obj
            and self.source_obj["subtype"] == "bot_message"
            or "bot_id" in self.source_obj
        ):
            return True
        else:
            return False

    @property
    def display_name(self):
        if hasattr(self, "source_obj"):
            # getting a display name for every typing event is incredibly wasteful
            if "user_typing" in self.source_obj.values():
                return self._display_name
            elif "profile" in self.source_obj:
                name_from_api = self.source_obj["profile"]["display_name_normalized"].lower()
                name_from_db = self._display_name
                if name_from_api != name_from_db:
                    self._display_name = name_from_api

        if not self._display_name:
            new_user = self.from_uid(self.uid)
            new_user.save()

        return self._display_name

    def grouper(self, n, iterable, padvalue=None):
        return zip(*[chain(iterable, repeat(padvalue, n - 1))] * n)

    @property
    def chroma_hash(self):
        name = self.uid

        hashed_name = hashlib.sha1(name.encode()).hexdigest()
        grouped_hash = [hashed_name[i : i + 6] for i in range(0, len(hashed_name), 6)]
        first_color = colored.bg(x256.from_hex(grouped_hash[0]))
        second_color = colored.bg(x256.from_hex(grouped_hash[1]))
        third_color = colored.bg(x256.from_hex(grouped_hash[2]))
        reset = colored.attr("reset")
        return f"{first_color} {reset}{second_color} {reset}{third_color} {reset}"
