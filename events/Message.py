import pprint
from datetime import datetime
import time
import colored
import settings
from x256 import x256

from events.ReplyableEvent import ReplyableEvent
from models.User import User


class Message(ReplyableEvent):
    expected_type = "message"

    def __init__(self, raw_line=None, channel=None, ts=None):
        self._channel = channel
        if channel and ts:
            raw_line = self.channel_and_ts_to_raw_line(channel, ts)

        super().__init__(raw_line)
        if "message" in raw_line:
            self.raw_line = raw_line["message"]
            self.original_line = raw_line  # just in case we want it later, y'know?
        elif "previous_message" in self.raw_line:  # oof idk about this one
            # this is for message-edited events
            self.raw_line = raw_line["previous_message"]
            self.original_line = raw_line

        if "text" in self.raw_line:
            self.text = self.raw_line["text"]

        if "reply_count" in self.raw_line:
            # the parents of threads get re-sent as an update
            # every time anyone replies to it. This is an override
            # so that you don't re-trigger a command forever
            self.text = "THREAD_PARENT"
            self.is_thread_parent = True

    @property
    def raw_timestamp(self):
        return self.raw_line["ts"]

    @property
    def timestamp(self):
        return datetime.fromtimestamp(float(self.raw_timestamp))

    @property
    def sender(self):
        if not self._sender:
            self._sender = User.from_source_object(self, create=True)
        return self._sender

    def __repr__(self):
        DEBUG = False
        if DEBUG:
            return pprint.pformat(self.raw_line)
        return f"{self.sender.chroma_hash}{self.timestamp.strftime('%H:%M')} - {self.sender.display_name}: {self.text}"
