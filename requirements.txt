# None of these are version-pegged
# This is an intentional decision
# It might be a bad one.

pip

# TEMPORARY BUGFIX VERSIONS
urllib3<1.25,>=1.21.1

slackclient<2
gevent
python-dateutil
colored
x256
sentry-sdk
sqlalchemy >= 1.3.0b1
lxml
cssselect
boto3
PyMySQL
python-gitlab
sphinx
cryptography
unidecode
pynamodb
pyzmq
timeago
dateparser
requests

# bugfix for slack RTM
# https://github.com/slackapi/python-slackclient/issues/189
websocket-client==0.44.0

