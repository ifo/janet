from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from JanetNeuron import JanetNeuron
from models.User import User
import datetime


class Learn(Model):
    class Meta:
        table_name = f"{JanetNeuron().workspace_name}_learns"

    text = UnicodeAttribute(range_key=True)
    target = UnicodeAttribute(hash_key=True)  # The target is the second word, usually a userid
    created_date = UTCDateTimeAttribute(default=datetime.datetime.utcnow)
    _learned_by = UnicodeAttribute(default="USLACKBOT")  # should never be default

    @property
    def learned_by(self):
        return User.from_uid(self._learned_by)

    def __repr__(self):
        return self.text


if not Learn.exists():
    Learn.create_table(read_capacity_units=1, write_capacity_units=1)
