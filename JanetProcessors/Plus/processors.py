from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import allow_events
from events.Message import Message
from events.ReactionAdded import ReactionAdded
from .models import PlusTarget

from models.User import User


class Plus(JanetProcessor):
    add_plus_words = ("?+ ", "?plus ", "?++ ", "?pluslearn")
    get_plus_words = ("?plusses", "?pluses")
    add_plus_reacts = ("heavy_plus_sign", "plusone", "pluslearn", "plus_mercy")

    @allow_events(Message, ReactionAdded)
    def process(self):
        event_type = type(self.event)
        is_message = self.is_message = event_type == Message
        is_react = self.is_react = event_type == ReactionAdded
        type_correct = is_message or is_react
        self.should_print_plusses = (
            should_print_plusses
        ) = is_message and self.event.text.startswith(self.get_plus_words)

        if type_correct and not self.event.sender.is_bot:
            if is_message:
                should_add_plus = self.event.text.startswith(self.add_plus_words) and (
                    len(self.plus_target_strings) > 0
                )
            if is_react:
                should_add_plus = self.event.reaction in self.add_plus_reacts

            if should_add_plus:
                plussed_targets = self.donate_plus(*self.plus_target_strings)
                print_plus_count = plussed_targets
            else:
                print_plus_count = self.plus_targets

            if should_print_plusses or should_add_plus:
                for target in print_plus_count:
                    maybe_user = User.from_uid(target.target)
                    if maybe_user:
                        target_name = maybe_user.display_name
                    else:
                        target_name = target.target
                    up_msg = f"{target_name} :arrow_up: {target.plus_count}"
                    down_msg = f":arrow_down: {PlusTarget.get(self.event.sender.uid).plus_count} {self.event.sender.display_name}"
                    self.event.reply_thread(f"{up_msg} / {down_msg}")

    @property
    def plus_target_strings(self):
        target_strings = []
        targets = []
        if self.is_message and self.args:
            maybe_user = None
            for target in self.args:
                # Try to make a user out of the string we've been given
                # Transform uid from display format to raw
                # i.e. <@USLACKBUTT> -> USLACKBUTT
                maybe_user = User.from_uid(target)
                if not maybe_user:
                    maybe_user = User.from_display_name(" ".join(self.args))

                if maybe_user:
                    target_strings.append(maybe_user.uid)

        elif self.is_react:
            reacted_to_user_uid = self.event.reacted_to_user.uid
            target_strings.append(reacted_to_user_uid)

        if not any(target_strings):
            if not self.args:
                return []
            else:
                target_strings.append(" ".join(self.args))

        return target_strings

    @property
    def plus_targets(self):
        target_strings = self.plus_target_strings
        target_objects = []
        for s in target_strings:
            try:
                new_obj = PlusTarget.get(s)
            except PlusTarget.DoesNotExist:
                new_obj = PlusTarget(s)
            target_objects.append(new_obj)

        return target_objects

    def donate_plus(self, *args):
        self.remove_plus(self.event.sender.uid, minus_count=len(args))
        return self.add_plus(*args)

    def remove_plus(self, *args, minus_count=1):
        minus_count = minus_count * -1
        return self.add_plus(*args, plus_count=minus_count)

    def add_plus(self, *args, plus_count=1):
        if self.event.sender.uid in args and plus_count > 0:
            args = list(args).remove(self.event.sender.uid)
            self.reply("Nah, you can't plus yourself")

        if not args:
            return []

        plussed_targets = []
        for target_string in args:
            try:
                target = PlusTarget.get(target_string)
            except PlusTarget.DoesNotExist:
                target = PlusTarget(target_string)
            target.plus_count += plus_count
            target.save()
            plussed_targets.append(target)
        return plussed_targets

    def show_all_plusses():
        pass


## TODO: test these things
# 1. plus other user via message
# 2. plus other via react
# 3. plus self via message & fail
# 4. plus self via react and fail
# 5. plus via display name
# 6. get plusses of other user
# 7. get plusses of self
# 8. donation works (check zero-sum)
