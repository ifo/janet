from JanetProcessors.JanetProcessor import JanetProcessor
from JanetProcessors.decorators import i_contains_words, no_bots, sampled_at
import re


class GenderCounter(JanetProcessor):
    gender_words = ("gender", "trans", "nonbinary", "queer")

    @i_contains_words(*gender_words)
    @sampled_at(0.05)
    @no_bots()
    def process(self):
        if (
            not "renamed the channel from" in self.event.text
            and self.the_gender_channel
        ):
            self.rename_channel(self.the_gender_channel["id"], self.next_chan_name)

    @property
    def next_chan_name(self):
        chan_num = self.the_gender_channel["name"].split("-")[1]
        next_num = hex(int(chan_num, 16) + 0x1)
        new_name = f"the-{next_num}-genders"
        return new_name

    @property
    def the_gender_channel(self):
        channel_name_regex = "the-0x[0-9a-f]*-genders"
        gender_channels = [
            c for c in self.channels if re.match(channel_name_regex, c["name"])
        ]
        if gender_channels:
            return gender_channels[0]
